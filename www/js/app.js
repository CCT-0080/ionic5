// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.config(function($stateProvider,$urlRouterProvider){

  $stateProvider
  .state('cadastro',{
    url: '/cadastro',
    templateUrl: 'cadastro.html',
    controller: 'appctrl'
  })
  .state('listar',{
    cache: false ,
    url: '/listar',
    templateUrl: 'listar.html',
    controller: 'appctrl'
  })
  .state('detalhar',{
    url: '/detalhar',
    templateUrl: 'detalhar.html',
    controller: 'appctrl'
  });
  $urlRouterProvider.otherwise('/cadastro');
});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
});

app.controller('appctrl', function($scope,$ionicPopup){

  $scope.pessoa = {};
  $scope.pessoas = [];

  $scope.carregarTodos = function(){
      var keys = Object.keys(window.localStorage);
      keys.forEach(function(item){
        var obj = JSON.parse(window.localStorage.getItem(item));
        $scope.pessoas.push(obj);
      });
  } 

  $scope.cadastrar = function(){
    if (angular.isDefined($scope.pessoa.nome) && angular.isDefined($scope.pessoa.sobrenome)){
      window.localStorage.setItem($scope.pessoa.nome,angular.toJson({nome: $scope.pessoa.nome,sobrenome: $scope.pessoa.sobrenome}));
 
      $ionicPopup.alert({
        title: "Cadastro",
        template: "Salvo com sucesso!!"
      });
    }
    $scope.pessoa.nome=undefined;
    $scope.pessoa.sobrenome=undefined;
    //location.href="#/listar";
    //location.reload();
  }

  $scope.detalhar = function(index){
    window.sessionStorage.setItem('indexValor',index);
  }

  $scope.mostrar = function(){
    var keys = Object.keys(window.localStorage);
    $scope.pessoa = JSON.parse(window.localStorage.getItem(keys[window.sessionStorage.getItem('indexValor')]));
  }

  $scope.excluir = function(){
    var keys = Object.keys(window.localStorage);
    window.localStorage.removeItem(keys[window.sessionStorage.getItem('indexValor')]);
  }
  
  $scope.carregarTodos();

});
